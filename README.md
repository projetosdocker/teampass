# Teampass

Teampass é um Gerenciador de senhas colaborativo

### Como executar
Caso não existir ainda, deve ser criado a persistência de dados:
```
mkdir /srv/deploy/teampass/bd
mkdir /srv/deploy/teampass/www
```

Caso não tiver sido criado, deve ser criado a rede:
```
docker network create --driver=overlay traefik-net
```
